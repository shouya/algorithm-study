
# Why to study algorithm?
A short answer: interest.

A longer answer: I am interested on computer science; in the study
process of different algorithm I find a lot of fun.

An even longer answer: algorithms are beautiful, are the part in
computer science which is as perfect as mathematics in the cosmos.


# Why coursera?
Simply, it's open for everyone. I love and approve the spirit of
opening. Besides, the websites offers the lecturers from famous
universities(most of them are professors), so the quanlity could be
guaranteed.

# Why this repository?
The website and the lectures freely sharing their knowledge and
experiences. So the same should I do and I am glad to do.

I use this repo to store my notes and ideas in the courses studying,
in which I record my confusions, understanding, thinking of the
course, I hope this will help the others students.

# What License?
![Creative Common License](https://i.creativecommons.org/l/by-sa/3.0/80x15.png)
This work is licensed under a Creative Commons Attribution-ShareAlike
3.0 Unported License.
(http://creativecommons.org/licenses/by-sa/3.0/)

Copyright (c) Shou Ya, 2013

