
Lecture URL: https://class.coursera.org/algo-003/lecture/20

# Notes
In the lesson professor Tim brought me a introduction of
algorithm. For the first example he used to arouse our interest, he
chose the **big number multiplication algorithm** as topic.

## The algorithm we learnt in primary school
```
       1 2 3 4
     * 5 6 7 8
 --------------
       9 8 7 2
     8 6 3 8 _
   7 4 0 4 _ _
 6 1 7 0 _ _ _
 --------------
 7 0 0 6 6 5 2
```

for the algorithm implemented in scheme, see `primary_school_alg.scm`.

To be noted that this implementation was really ugly as it is my first
practical scheme program. However the program could pass
(revo)[https://shouya.github.com/revo], plt-r5rs, and guile
interpreters.

It's very hard I felt actually to change my imperative programming
thinking to pure functional ones. Since you see there I can't help did
some emulation of imperative ones via the functional ways.

Anyway, just regard this as scheme learning.



