

(define one-digit-multiplication-table
  (list->vector
   (let rec-1 ((n 0))
     (cond ((= n 10) '())
	   (else (cons (list->vector
			(let rec-2 ((m 0))
			  (cond ((= m 10) '())
				(else (cons (* m n) (rec-2 (+ m 1)))))))
		       (rec-1 (+ n 1))))))))

(define (one-digit-multiply a b)
  (vector-ref (vector-ref one-digit-multiplication-table a) b))

(define odm one-digit-multiply)

;; uncomment the following line when using revo
(define quotient /)

(define first-number 1234)
(define second-number 5678)

(define (break-number-into-digits n-digit-num)
  (letrec ((xxx (lambda (x)
		  (cond ((= x 0) '())
			(else (cons (remainder x 10)
				    (xxx (quotient x 10))))))))
    (reverse (xxx n-digit-num))))


(define trans-table (map
		     (lambda (n)
		       (let ((carry 0)
			     (line '()))
			 (for-each
			  (lambda (m)
			    (let* ((product (+ (odm m n) carry))
				   (rem (remainder product 10)))
			      (set! carry (quotient product 10))
			      (set! line (cons rem line))))
			  (reverse (break-number-into-digits first-number)))
			 (cons carry line)))
		     (reverse (break-number-into-digits second-number))))

;(display trans-table)

(define (ladder-shift lol)
  (if (null? lol) '()
      (cons (car lol)
	    (map (lambda (x) (append x '(0))) (ladder-shift (cdr lol))))))

(define (align-fill lol)
  (letrec ((maxlen (let maxlen ((list lol))
		     (if (null? list) 0
			 (let ((this (length (car list)))
			       (rest (maxlen (cdr list))))
			   (if (< this rest) rest this)))))
	   (align (lambda (list target-length)
		    (if (= (length list) target-length) list
			(align (cons 0 list) target-length)))))
    (map (lambda (x) (align x maxlen)) lol)))

(set! trans-table (align-fill (ladder-shift trans-table)))


(define (transpose lol) ; lol: list of list
  (apply map list lol))

;  (if (null? (car lol)) '()
;      (cons (map car lol) (transpose (map cdr lol)))))


(define digits-sum (transpose (map reverse (reverse trans-table))))

(define (fold-left proc init list)
  (if (= (length list) 1) (proc init (car list))
      (proc (fold-left proc init (cdr list)) (car list))))


(define raw-result 0)
(let ((carry 0)
      (list '()))
  (map (lambda (x)
	 (let* ((result (+ (fold-left + 0 x) carry))
		(rem (remainder result 10))
		(quo (quotient result 10)))
	   (set! carry quo)
	   (set! list (cons rem list))))
       digits-sum)
  (set! list (cons carry list))
  (set! raw-result list))

(define (strip-prefix-zeros list)
  (if (= (car list) 0) (strip-prefix-zeros (cdr list))
      list))

(define result (strip-prefix-zeros raw-result))
(display result)
(newline)




